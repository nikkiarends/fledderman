Drupal.locale = { 'pluralFormula': function ($n) { return Number(($n!=1)); }, 'strings': {"":{"An AJAX HTTP error occurred.":"Er is een AJAX HTTP fout opgetreden.","HTTP Result Code: !status":"HTTP-resultaatcode: !status","An AJAX HTTP request terminated abnormally.":"Een AJAX HTTP-aanvraag is onverwacht afgebroken","Debugging information follows.":"Debug informatie volgt.","Path: !uri":"Pad: !uri","StatusText: !statusText":"Statustekst: !statusText","ResponseText: !responseText":"Antwoordtekst: !responseText","ReadyState: !readyState":"ReadyState: !readyState","Hide":"Verbergen","Show":"Weergeven","Show shortcuts":"Snelkoppelingen weergeven","Hide shortcuts":"Snelkoppelingen verbergen","Re-order rows by numerical weight instead of dragging.":"Herschik de rijen op basis van gewicht, in plaats van slepen.","Show row weights":"Gewicht van rijen tonen","Hide row weights":"Gewicht van rij verbergen","Drag to re-order":"Slepen om de volgorde te wijzigen","Changes made in this table will not be saved until the form is submitted.":"Wijzigingen in deze tabel worden pas opgeslagen wanneer het formulier wordt ingediend.","Disabled":"Uitgeschakeld","Cancel":"Annuleren","OK":"Ok","Configure":"Instellen","Next":"Volgende","Edit":"Bewerken","none":"geen","Add":"Toevoegen","This field is required.":"Dit veld is verplicht.","Allowed HTML tags":"Toegestane HTML-tags","Select all rows in this table":"Selecteer alle regels van deze tabel","Deselect all rows in this table":"De-selecteer alle regels van deze tabel","Not published":"Niet gepubliceerd","Please wait...":"Even geduld...","By @name on @date":"Door @name op @date","By @name":"Door @name","Not in menu":"Niet in een menu","Alias: @alias":"Alias: @alias","No alias":"Geen alias","New revision":"Nieuwe revisie","The changes to these blocks will not be saved until the \u003Cem\u003ESave blocks\u003C\/em\u003E button is clicked.":"Wijzigingen aan de blokken worden pas opgeslagen wanneer u de knop \u003Cem\u003EBlokken opslaan\u003C\/em\u003E aanklikt.","This permission is inherited from the authenticated user role.":"Dit toegangsrecht is ge\u00ebrfd van de rol \u0027geverifieerde gebruiker\u0027.","No revision":"Geen revisie","Requires a title":"Een titel is verplicht","Not restricted":"Geen beperking","(active tab)":"(actieve tabblad)","Not customizable":"Niet aanpasbaar","Restricted to certain pages":"Beperkt tot bepaalde pagina\u0027s","The block cannot be placed in this region.":"Het blok kan niet worden geplaatst in dit gebied.","Hide summary":"Samenvatting verbergen","Edit summary":"Samenvatting bewerken","Don\u0027t display post information":"Geen berichtinformatie weergeven","The selected file %filename cannot be uploaded. Only files with the following extensions are allowed: %extensions.":"Het bestand %filename kan niet ge\u00fcpload worden. Alleen bestanden met de volgende extensies zijn toegestaan: %extensions","Autocomplete popup":"Popup voor automatisch aanvullen","Searching for matches...":"Zoeken naar overeenkomsten...","@label: @value":"@label: @value","Automatic alias":"Automatische alias","Available tokens":"Beschikbare tokens","Insert this token into your form":"Plaats deze token in uw formulier","First click a text field to insert your tokens into.":"Klik eerst een tekstveld aan om uw tokens in te plaatsen.","Select":"Selecteren","Remove group":"Groep verwijderen","Apply (all displays)":"Toepassen (alle weergaven)","Apply (this display)":"Toepassen (deze weergave)","Revert to default":"Terugzetten naar standaard","Submit":"Indienen","Inclusion: @value":"Insluiting: @value","Priority: @value":"Prioriteit: @value","Loading token browser...":"Tokenbrowser laden...","Previous":"Vorige","Loading...":"Bezig met laden...","Media browser":"Mediabrowser","all":"alle","Cannot continue, nothing selected":"Kan niet doorgaan, niets geselecteerd","Error getting media.":"Fout bij het ophalen van media.","Close":"Sluiten","Pause Slideshow":"Slideshow pauzeren","Play Slideshow":"Slideshow afspelen"}} };;
/* $Id: lightbox_lite.js,v 1.1.2.2.2.19 2010/06/07 14:54:30 snpower Exp $ */

/**
 * Lightbox JS: Fullsize Image Overlays
 * by Lokesh Dhakar - http://www.huddletogether.com
 *
 * For more information on this script, visit:
 * http://huddletogether.com/projects/lightbox/
 *
 * This script is distributed via Drupal.org with permission from Lokesh Dhakar.
 * Under GPL license.
 *    Mailto: bugzie@gmail.com
 */

// start jQuery block
(function ($) {
//
// getPageScroll()
// Returns array with x,y page scroll values.
// Core code from - quirksmode.org
//
function getPageScroll() {

  var xScroll, yScroll;

  if (self.pageYOffset) {
    yScroll = self.pageYOffset;
    xScroll = self.pageXOffset;

  // Explorer 6 Strict
  }
  else if (document.documentElement && document.documentElement.scrollTop) {
    yScroll = document.documentElement.scrollTop;
    xScroll = document.documentElement.scrollLeft;
  }
  else if (document.body) {// all other Explorers
    yScroll = document.body.scrollTop;
    xScroll = document.body.scrollLeft;
  }

  arrayPageScroll = [xScroll, yScroll];
  return arrayPageScroll;
}



// getPageSize()
// Returns array with page width, height and window width, height
// Core code from - quirksmode.org
// Edit for Firefox by pHaez
function getPageSize() {

  var xScroll, yScroll;

  if (window.innerHeight && window.scrollMaxY) {
    xScroll = window.innerWidth + window.scrollMaxX;
    yScroll = window.innerHeight + window.scrollMaxY;
  // all but Explorer Mac
  }
  else if (document.body.scrollHeight > document.body.offsetHeight) {
    xScroll = document.body.scrollWidth;
    yScroll = document.body.scrollHeight;
  // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
  }
  else {
    xScroll = document.body.offsetWidth;
    yScroll = document.body.offsetHeight;
  }

  var windowWidth, windowHeight;
  if (self.innerHeight) { // all except Explorer
    if (document.documentElement.clientHeight) {
      windowWidth = document.documentElement.clientWidth;
    }
    else {
      windowWidth = self.innerWidth;
    }
    windowHeight = self.innerHeight;
  }
  else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
    windowWidth = document.documentElement.clientWidth;
    windowHeight = document.documentElement.clientHeight;
  }
  else if (document.body) { // other Explorers
    windowWidth = document.body.clientWidth;
    windowHeight = document.body.clientHeight;
  }

  // for small pages with total height less then height of the viewport
  if (yScroll < windowHeight) {
    pageHeight = windowHeight;
  }
  else {
    pageHeight = yScroll;
  }

  // for small pages with total width less then width of the viewport
  if (xScroll < windowWidth) {
    pageWidth = xScroll;
  }
  else {
    pageWidth = windowWidth;
  }


  arrayPageSize = [pageWidth, pageHeight, windowWidth, windowHeight];
  return arrayPageSize;
}


// pause(numberMillis)
function pause(ms) {
  var date = new Date();
  var curDate = null;
  do { curDate = new Date(); }
  while (curDate - date < ms);
}

// hideLightbox()
function hideLightbox() {
  // get objects
  objOverlay = document.getElementById('lightbox2-overlay');
  objLightbox = document.getElementById('lightbox');

  // hide lightbox and overlay
  objOverlay.style.display = 'none';
  objLightbox.style.display = 'none';

  // make select boxes visible
  selects = document.getElementsByTagName("select");
  for (i = 0; i != selects.length; i++) {
    if (selects[i].style.display != "none") {
      selects[i].style.visibility = "visible";
    }
  }

  // make flash objects visible
  embed = document.getElementsByTagName("embed");
  for (i = 0; i != embed.length; i++) {
    if (embed[i].style.display != "none") {
      embed[i].style.visibility = "visible";
    }
  }
  objects = document.getElementsByTagName("object");
  for (i = 0; i != objects.length; i++) {
    if (objects[i].style.display != "none") {
      objects[i].style.visibility = "visible";
    }
  }

  // disable keydown listener
  document.onkeydown = '';
}


// getKey(key)
// Gets keycode. If 'x' is pressed then it hides the lightbox.
function getKey(e) {
  if (e === null) { // ie
    keycode = event.keyCode;
    escapeKey = 27;
  }
  else { // mozilla
    keycode = e.keyCode;
    escapeKey = e.DOM_VK_ESCAPE;
  }
  key = String.fromCharCode(keycode).toLowerCase();

  if (key == 'x' || key == 'c' || keycode == escapeKey) { hideLightbox(); }
}


// listenKey()
function listenKey () { document.onkeydown = getKey; }


function imgLoadingError(image, objImage, objLink) {
  var settings = Drupal.settings.lightbox2;
  image.src = settings.default_image;
  objImage.src = settings.default_image;
  objLink.href = settings.default_image;
}


// showLightbox()
// Preloads images. Pleaces new image in lightbox then centers and displays.
function showLightbox(objLink) {
  var settings = Drupal.settings.lightbox2;
  // prep objects
  var objOverlay = document.getElementById('lightbox2-overlay');
  var objLightbox = document.getElementById('lightbox');
  var objCaption = document.getElementById('lightboxCaption');
  var objImage = document.getElementById('lightboxImage');
  var objLoadingImage = document.getElementById('loadingImage');
  var objLightboxDetails = document.getElementById('lightboxDetails');

  var arrayPageSize = getPageSize();
  var arrayPageScroll = getPageScroll();

  // set height of Overlay to take up whole page and show
  objOverlay.style.height = (arrayPageSize[1] + 'px');
  objOverlay.style.display = 'block';
  objOverlay.style.opacity = settings.overlay_opacity;
  objOverlay.style.backgroundColor = '#' + settings.overlay_color;

  // preload image
  imgPreload = new Image();
  imgPreload.onerror = function() { imgLoadingError(this, objImage, objLink); };

  imgPreload.onload = function() {
    objImage.src = objLink.href;

    // center lightbox and make sure that the top and left values are not
    // negative and the image placed outside the viewport
    var lightboxTop = arrayPageScroll[1] + ((arrayPageSize[3] - 35 - imgPreload.height) / 2);
    var lightboxLeft = ((arrayPageSize[0] - 20 - imgPreload.width) / 2);

    objLightbox.style.top = (lightboxTop < 0) ? "0px" : lightboxTop + "px";
    objLightbox.style.left = (lightboxLeft < 0) ? "0px" : lightboxLeft + "px";


    //objLightboxDetails.style.width = imgPreload.width + 'px';
    objLightbox.style.width = imgPreload.width + 'px';

    if (objLink.getAttribute('title')) {
      objCaption.style.display = 'block';
      //objCaption.style.width = imgPreload.width + 'px';
      objCaption.innerHTML = objLink.getAttribute('title');
    }
    else {
      objCaption.style.display = 'none';
    }

    // A small pause between the image loading and displaying is required with
    // IE,  this prevents the previous image displaying for a short burst
    // causing flicker.
    if (navigator.appVersion.indexOf("MSIE") != -1) {
      pause(250);
    }

    if (objLoadingImage) { objLoadingImage.style.display = 'none'; }

    // Hide select boxes as they will 'peek' through the image in IE
    selects = document.getElementsByTagName("select");
    for (i = 0; i != selects.length; i++) {
      if (selects[i].style.display != "none") {
        selects[i].style.visibility = "hidden";
      }
    }

    // Hide flash objects as they will 'peek' through the image in IE
    embed = document.getElementsByTagName("embed");
    for (i = 0; i != embed.length; i++) {
      if (embed[i].style.display != "none") {
        embed[i].style.visibility = "hidden";
      }
    }
    objects = document.getElementsByTagName("object");
    for (i = 0; i != objects.length; i++) {
      if (objects[i].style.display != "none") {
        objects[i].style.visibility = "hidden";
      }
    }

    objLightbox.style.display = 'block';

    // After image is loaded, update the overlay height as the new image might
    // have increased the overall page height.
    arrayPageSize = getPageSize();
    objOverlay.style.height = (arrayPageSize[1] + 'px');

    // Check for 'x' keydown
    listenKey();

    return false;
  };

  imgPreload.src = objLink.href;

}



// initLightbox()
// Function runs on window load, going through link tags looking for
// rel="lightbox".  These links receive onclick events that enable the lightbox
// display for their targets.  The function also inserts html markup at the top
// of the page which will be used as a container for the overlay pattern and
// the inline image.
function initLightbox() {

  if (!document.getElementsByTagName) { return; }
  var anchors = document.getElementsByTagName("a");

  // loop through all anchor tags
  for (var i = 0; i < anchors.length; i++) {
    var anchor = anchors[i];
    var relAttribute = String(anchor.getAttribute("rel"));

    if (anchor.getAttribute("href") && relAttribute.toLowerCase().match("lightbox")) {
      $(anchor).click(function(e) { showLightbox(this); if (e.preventDefault) { e.preventDefault(); } return false; });
    }
  }

  // the rest of this code inserts html at the top of the page that looks like
  // this:
  // <div id="lightbox2-overlay">
  //  <a href="#" onclick="hideLightbox(); return false;"><img id="loadingImage" /></a>
  // </div>
  // <div id="lightbox">
  //  <a href="#" onclick="hideLightbox(); return false;" title="Click anywhere to close image">
  //   <img id="closeButton" />
  //   <img id="lightboxImage" />
  //  </a>
  //  <div id="lightboxDetails">
  //   <div id="lightboxCaption"></div>
  //   <div id="keyboardMsg"></div>
  //  </div>
  // </div>

  var objBody = document.getElementsByTagName("body").item(0);

  // create overlay div and hardcode some functional styles (aesthetic styles are in CSS file)
  var objOverlay = document.createElement("div");
  objOverlay.setAttribute('id', 'lightbox2-overlay');
  objOverlay.onclick = function () { hideLightbox(); return false; };
  objOverlay.style.display = 'none';
  objOverlay.style.position = 'absolute';
  objOverlay.style.top = '0';
  objOverlay.style.left = '0';
  objOverlay.style.zIndex = '90';
  objOverlay.style.width = '100%';
  objBody.insertBefore(objOverlay, objBody.firstChild);

  var arrayPageSize = getPageSize();
  var arrayPageScroll = getPageScroll();

  // create loader image
  var objLoadingImage = document.createElement("span");
  objLoadingImage.setAttribute('id', 'loadingImage');
  objOverlay.appendChild(objLoadingImage);

  // create lightbox div, same note about styles as above
  var objLightbox = document.createElement("div");
  objLightbox.setAttribute('id', 'lightbox');
  objLightbox.style.display = 'none';
  objLightbox.style.position = 'absolute';
  objLightbox.style.zIndex = '100';
  objBody.insertBefore(objLightbox, objOverlay.nextSibling);

  // create link
  var objLink = document.createElement("a");
  objLink.setAttribute('href', '#');
  objLink.setAttribute('title', 'Click to close');
  objLink.onclick = function () { hideLightbox(); return false; };
  objLightbox.appendChild(objLink);

  // create close button image
  var objCloseButton = document.createElement("span");
  objCloseButton.setAttribute('id', 'closeButton');
  objLink.appendChild(objCloseButton);

  // create image
  var objImage = document.createElement("img");
  objImage.setAttribute('id', 'lightboxImage');
  objLink.appendChild(objImage);

  // create details div, a container for the caption and keyboard message
  var objLightboxDetails = document.createElement("div");
  objLightboxDetails.setAttribute('id', 'lightboxDetails');
  objLightbox.appendChild(objLightboxDetails);

  // create caption
  var objCaption = document.createElement("div");
  objCaption.setAttribute('id', 'lightboxCaption');
  objCaption.style.display = 'none';
  objLightboxDetails.appendChild(objCaption);

  // create keyboard message
  var settings = Drupal.settings.lightbox2;
  var objKeyboardMsg = document.createElement("div");
  objKeyboardMsg.setAttribute('id', 'keyboardMsg');
  objKeyboardMsg.innerHTML = settings.lite_press_x_close;
  objLightboxDetails.appendChild(objKeyboardMsg);
}

Drupal.behaviors.initLightbox2 = {
  attach: function(context) {
    initLightbox();
  }
};

//End jQuery block
}(jQuery));
;
(function ($) {

/**
 * Toggle the visibility of a fieldset using smooth animations.
 */
Drupal.toggleFieldset = function (fieldset) {
  var $fieldset = $(fieldset);
  if ($fieldset.is('.collapsed')) {
    var $content = $('> .fieldset-wrapper', fieldset).hide();
    $fieldset
      .removeClass('collapsed')
      .trigger({ type: 'collapsed', value: false })
      .find('> legend span.fieldset-legend-prefix').html(Drupal.t('Hide'));
    $content.slideDown({
      duration: 'fast',
      easing: 'linear',
      complete: function () {
        Drupal.collapseScrollIntoView(fieldset);
        fieldset.animating = false;
      },
      step: function () {
        // Scroll the fieldset into view.
        Drupal.collapseScrollIntoView(fieldset);
      }
    });
  }
  else {
    $fieldset.trigger({ type: 'collapsed', value: true });
    $('> .fieldset-wrapper', fieldset).slideUp('fast', function () {
      $fieldset
        .addClass('collapsed')
        .find('> legend span.fieldset-legend-prefix').html(Drupal.t('Show'));
      fieldset.animating = false;
    });
  }
};

/**
 * Scroll a given fieldset into view as much as possible.
 */
Drupal.collapseScrollIntoView = function (node) {
  var h = document.documentElement.clientHeight || document.body.clientHeight || 0;
  var offset = document.documentElement.scrollTop || document.body.scrollTop || 0;
  var posY = $(node).offset().top;
  var fudge = 55;
  if (posY + node.offsetHeight + fudge > h + offset) {
    if (node.offsetHeight > h) {
      window.scrollTo(0, posY);
    }
    else {
      window.scrollTo(0, posY + node.offsetHeight - h + fudge);
    }
  }
};

Drupal.behaviors.collapse = {
  attach: function (context, settings) {
    $('fieldset.collapsible', context).once('collapse', function () {
      var $fieldset = $(this);
      // Expand fieldset if there are errors inside, or if it contains an
      // element that is targeted by the URI fragment identifier.
      var anchor = location.hash && location.hash != '#' ? ', ' + location.hash : '';
      if ($fieldset.find('.error' + anchor).length) {
        $fieldset.removeClass('collapsed');
      }

      var summary = $('<span class="summary"></span>');
      $fieldset.
        bind('summaryUpdated', function () {
          var text = $.trim($fieldset.drupalGetSummary());
          summary.html(text ? ' (' + text + ')' : '');
        })
        .trigger('summaryUpdated');

      // Turn the legend into a clickable link, but retain span.fieldset-legend
      // for CSS positioning.
      var $legend = $('> legend .fieldset-legend', this);

      $('<span class="fieldset-legend-prefix element-invisible"></span>')
        .append($fieldset.hasClass('collapsed') ? Drupal.t('Show') : Drupal.t('Hide'))
        .prependTo($legend)
        .after(' ');

      // .wrapInner() does not retain bound events.
      var $link = $('<a class="fieldset-title" href="#"></a>')
        .prepend($legend.contents())
        .appendTo($legend)
        .click(function () {
          var fieldset = $fieldset.get(0);
          // Don't animate multiple times.
          if (!fieldset.animating) {
            fieldset.animating = true;
            Drupal.toggleFieldset(fieldset);
          }
          return false;
        });

      $legend.append(summary);
    });
  }
};

})(jQuery);
;
(function ($) {

/**
 * Attaches sticky table headers.
 */
Drupal.behaviors.tableHeader = {
  attach: function (context, settings) {
    if (!$.support.positionFixed) {
      return;
    }

    $('table.sticky-enabled', context).once('tableheader', function () {
      $(this).data("drupal-tableheader", new Drupal.tableHeader(this));
    });
  }
};

/**
 * Constructor for the tableHeader object. Provides sticky table headers.
 *
 * @param table
 *   DOM object for the table to add a sticky header to.
 */
Drupal.tableHeader = function (table) {
  var self = this;

  this.originalTable = $(table);
  this.originalHeader = $(table).children('thead');
  this.originalHeaderCells = this.originalHeader.find('> tr > th');
  this.displayWeight = null;

  // React to columns change to avoid making checks in the scroll callback.
  this.originalTable.bind('columnschange', function (e, display) {
    // This will force header size to be calculated on scroll.
    self.widthCalculated = (self.displayWeight !== null && self.displayWeight === display);
    self.displayWeight = display;
  });

  // Clone the table header so it inherits original jQuery properties. Hide
  // the table to avoid a flash of the header clone upon page load.
  this.stickyTable = $('<table class="sticky-header"/>')
    .insertBefore(this.originalTable)
    .css({ position: 'fixed', top: '0px' });
  this.stickyHeader = this.originalHeader.clone(true)
    .hide()
    .appendTo(this.stickyTable);
  this.stickyHeaderCells = this.stickyHeader.find('> tr > th');

  this.originalTable.addClass('sticky-table');
  $(window)
    .bind('scroll.drupal-tableheader', $.proxy(this, 'eventhandlerRecalculateStickyHeader'))
    .bind('resize.drupal-tableheader', { calculateWidth: true }, $.proxy(this, 'eventhandlerRecalculateStickyHeader'))
    // Make sure the anchor being scrolled into view is not hidden beneath the
    // sticky table header. Adjust the scrollTop if it does.
    .bind('drupalDisplaceAnchor.drupal-tableheader', function () {
      window.scrollBy(0, -self.stickyTable.outerHeight());
    })
    // Make sure the element being focused is not hidden beneath the sticky
    // table header. Adjust the scrollTop if it does.
    .bind('drupalDisplaceFocus.drupal-tableheader', function (event) {
      if (self.stickyVisible && event.clientY < (self.stickyOffsetTop + self.stickyTable.outerHeight()) && event.$target.closest('sticky-header').length === 0) {
        window.scrollBy(0, -self.stickyTable.outerHeight());
      }
    })
    .triggerHandler('resize.drupal-tableheader');

  // We hid the header to avoid it showing up erroneously on page load;
  // we need to unhide it now so that it will show up when expected.
  this.stickyHeader.show();
};

/**
 * Event handler: recalculates position of the sticky table header.
 *
 * @param event
 *   Event being triggered.
 */
Drupal.tableHeader.prototype.eventhandlerRecalculateStickyHeader = function (event) {
  var self = this;
  var calculateWidth = event.data && event.data.calculateWidth;

  // Reset top position of sticky table headers to the current top offset.
  this.stickyOffsetTop = Drupal.settings.tableHeaderOffset ? eval(Drupal.settings.tableHeaderOffset + '()') : 0;
  this.stickyTable.css('top', this.stickyOffsetTop + 'px');

  // Save positioning data.
  var viewHeight = document.documentElement.scrollHeight || document.body.scrollHeight;
  if (calculateWidth || this.viewHeight !== viewHeight) {
    this.viewHeight = viewHeight;
    this.vPosition = this.originalTable.offset().top - 4 - this.stickyOffsetTop;
    this.hPosition = this.originalTable.offset().left;
    this.vLength = this.originalTable[0].clientHeight - 100;
    calculateWidth = true;
  }

  // Track horizontal positioning relative to the viewport and set visibility.
  var hScroll = document.documentElement.scrollLeft || document.body.scrollLeft;
  var vOffset = (document.documentElement.scrollTop || document.body.scrollTop) - this.vPosition;
  this.stickyVisible = vOffset > 0 && vOffset < this.vLength;
  this.stickyTable.css({ left: (-hScroll + this.hPosition) + 'px', visibility: this.stickyVisible ? 'visible' : 'hidden' });

  // Only perform expensive calculations if the sticky header is actually
  // visible or when forced.
  if (this.stickyVisible && (calculateWidth || !this.widthCalculated)) {
    this.widthCalculated = true;
    var $that = null;
    var $stickyCell = null;
    var display = null;
    var cellWidth = null;
    // Resize header and its cell widths.
    // Only apply width to visible table cells. This prevents the header from
    // displaying incorrectly when the sticky header is no longer visible.
    for (var i = 0, il = this.originalHeaderCells.length; i < il; i += 1) {
      $that = $(this.originalHeaderCells[i]);
      $stickyCell = this.stickyHeaderCells.eq($that.index());
      display = $that.css('display');
      if (display !== 'none') {
        cellWidth = $that.css('width');
        // Exception for IE7.
        if (cellWidth === 'auto') {
          cellWidth = $that[0].clientWidth + 'px';
        }
        $stickyCell.css({'width': cellWidth, 'display': display});
      }
      else {
        $stickyCell.css('display', 'none');
      }
    }
    this.stickyTable.css('width', this.originalTable.outerWidth());
  }
};

})(jQuery);
;
(function ($) {

Drupal.toolbar = Drupal.toolbar || {};

/**
 * Attach toggling behavior and notify the overlay of the toolbar.
 */
Drupal.behaviors.toolbar = {
  attach: function(context) {

    // Set the initial state of the toolbar.
    $('#toolbar', context).once('toolbar', Drupal.toolbar.init);

    // Toggling toolbar drawer.
    $('#toolbar a.toggle', context).once('toolbar-toggle').click(function(e) {
      Drupal.toolbar.toggle();
      // Allow resize event handlers to recalculate sizes/positions.
      $(window).triggerHandler('resize');
      return false;
    });
  }
};

/**
 * Retrieve last saved cookie settings and set up the initial toolbar state.
 */
Drupal.toolbar.init = function() {
  // Retrieve the collapsed status from a stored cookie.
  var collapsed = $.cookie('Drupal.toolbar.collapsed');

  // Expand or collapse the toolbar based on the cookie value.
  if (collapsed == 1) {
    Drupal.toolbar.collapse();
  }
  else {
    Drupal.toolbar.expand();
  }
};

/**
 * Collapse the toolbar.
 */
Drupal.toolbar.collapse = function() {
  var toggle_text = Drupal.t('Show shortcuts');
  $('#toolbar div.toolbar-drawer').addClass('collapsed');
  $('#toolbar a.toggle')
    .removeClass('toggle-active')
    .attr('title',  toggle_text)
    .html(toggle_text);
  $('body').removeClass('toolbar-drawer').css('paddingTop', Drupal.toolbar.height());
  $.cookie(
    'Drupal.toolbar.collapsed',
    1,
    {
      path: Drupal.settings.basePath,
      // The cookie should "never" expire.
      expires: 36500
    }
  );
};

/**
 * Expand the toolbar.
 */
Drupal.toolbar.expand = function() {
  var toggle_text = Drupal.t('Hide shortcuts');
  $('#toolbar div.toolbar-drawer').removeClass('collapsed');
  $('#toolbar a.toggle')
    .addClass('toggle-active')
    .attr('title',  toggle_text)
    .html(toggle_text);
  $('body').addClass('toolbar-drawer').css('paddingTop', Drupal.toolbar.height());
  $.cookie(
    'Drupal.toolbar.collapsed',
    0,
    {
      path: Drupal.settings.basePath,
      // The cookie should "never" expire.
      expires: 36500
    }
  );
};

/**
 * Toggle the toolbar.
 */
Drupal.toolbar.toggle = function() {
  if ($('#toolbar div.toolbar-drawer').hasClass('collapsed')) {
    Drupal.toolbar.expand();
  }
  else {
    Drupal.toolbar.collapse();
  }
};

Drupal.toolbar.height = function() {
  var $toolbar = $('#toolbar');
  var height = $toolbar.outerHeight();
  // In modern browsers (including IE9), when box-shadow is defined, use the
  // normal height.
  var cssBoxShadowValue = $toolbar.css('box-shadow');
  var boxShadow = (typeof cssBoxShadowValue !== 'undefined' && cssBoxShadowValue !== 'none');
  // In IE8 and below, we use the shadow filter to apply box-shadow styles to
  // the toolbar. It adds some extra height that we need to remove.
  if (!boxShadow && /DXImageTransform\.Microsoft\.Shadow/.test($toolbar.css('filter'))) {
    height -= $toolbar[0].filters.item("DXImageTransform.Microsoft.Shadow").strength;
  }
  return height;
};

})(jQuery);
;
