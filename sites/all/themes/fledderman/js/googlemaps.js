(function ($) {

$(function LoadGmaps() {
			
var styles = [
  {
    
  }
];
  
  var styledMap = new google.maps.StyledMapType(styles,
    {name: "Styled Map"});

// de bovenste van de 2 hieronder is de locatie van de marker. De 2e is het middenpunt van de kaart bij het laden
// De Latitude en Longitude kan je hier opvragen: http://itouchmap.com/latlong.html
				var myLatlng = new google.maps.LatLng(53.248119,6.859930);
                var center = new google.maps.LatLng(53.168520,6.601597);
			
// Hieronder kan je een icoontje neer zetten die gebruikt wordt als Marker.
				var markerImage = 'https://www.fledderman.nl/sites/all/themes/fledderman/images/marker.png';
				
				var myOptions = {

// hoe ver moet hij ingezoomd zijn bij het laden?
					zoom: 15,
					center: myLatlng,
					scrollwheel: false,
					disableDefaultUI: true,
					panControl: false,
					zoomControl: true,
					mapTypeControl: true,
					mapTypeControlOptions: {
						style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
					},
					streetViewControl: false,
					mapTypeId: google.maps.MapTypeId.ROADMAP
					}
					
					


					
				var map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
				
				  //Associate the styled map with the MapTypeId and set it to display.
  map.mapTypes.set('map_style', styledMap);
  map.setMapTypeId('map_style');
				
				var marker = new google.maps.Marker({
					position: myLatlng,
					map: map,
					title:"Autobedrijf Tulp",
			
// als je geen custom marker wil moet je deze hieronder weg halen
					icon: markerImage
				});
				var infowindow = new google.maps.InfoWindow({
			
// zet hier het berichtje in die je wil laten zien wanneer je op de marker klikt (html mag)
					content: "<div class='contactpopdiv' style='text-align:center;'>Hoofdweg 90<Br />9628 CR Siddeburen</div>"});
					google.maps.event.addListener(marker, "click", function() {
						infowindow.open(map, marker);
					});
                    //infowindow.open(map, marker);
			});
	
	 })(jQuery);
	
			//google.maps.event.addDomListener(window, 'load', LoadGmaps);
			