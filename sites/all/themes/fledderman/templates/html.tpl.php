<?php
/**
 * @file
 * Returns the HTML for the basic html structure of a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728208
 */
?><!DOCTYPE html>
<!--[if IEMobile 7]><html class="iem7" <?php print $html_attributes; ?>><![endif]-->
<!--[if lte IE 6]><html class="lt-ie9 lt-ie8 lt-ie7" <?php print $html_attributes; ?>><![endif]-->
<!--[if (IE 7)&(!IEMobile)]><html class="lt-ie9 lt-ie8" <?php print $html_attributes; ?>><![endif]-->
<!--[if IE 8]><html class="lt-ie9" <?php print $html_attributes; ?>><![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)]><!--><html <?php print $html_attributes . $rdf_namespaces; ?>><!--<![endif]-->

<head>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>

  <?php if ($default_mobile_metatags): ?>
    <meta name="MobileOptimized" content="width">
    <meta name="HandheldFriendly" content="true">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
  <?php endif; ?>
  <meta http-equiv="cleartype" content="on">

  <?php print $styles; ?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="https://www.fledderman.nl/sites/all/themes/fledderman/js/jquery-ui.min.js"></script>
  <script src="https://www.fledderman.nl/sites/all/themes/fledderman/js/jquery.matchHeight-min.js"></script>
  <script src="https://www.fledderman.nl/sites/all/themes/fledderman/js/jquery.cookie.js"></script>
  <script src="https://www.fledderman.nl/sites/all/themes/fledderman/js/min/custom-min.js"></script>
  <?php print $scripts; ?>
  <?php if ($add_html5_shim and !$add_respond_js): ?>
    <!--[if lt IE 9]>
    <script src="<?php print $base_path . $path_to_zen; ?>/js/html5.js"></script>
    <![endif]-->
  <?php elseif ($add_html5_shim and $add_respond_js): ?>
    <!--[if lt IE 9]>
    <script src="<?php print $base_path . $path_to_zen; ?>/js/html5-respond.js"></script>
    <![endif]-->
  <?php elseif ($add_respond_js): ?>
    <!--[if lt IE 9]>
    <script src="<?php print $base_path . $path_to_zen; ?>/js/respond.js"></script>
    <![endif]-->
  <?php endif; ?>
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:300,700|Convergence|Roboto:100,700,100italic' rel='stylesheet' type='text/css'>
    <script>

      $(document).ready(function() {
          if ($.cookie('noShowWelcomefledder2')) $('#popupmelding').hide();
          else {
              $("#popupmelding").click(function() {
                $.cookie('noShowWelcomefledder2', true);    
                
              });
              $(".popupsluiten").click(function() {
                  $.cookie('noShowWelcomefledder2', true);    
                  window.location.href = "http://www.fledderman.nl/subsidie";
              });
          }
      });
         $(document).ready(function(){ 
            $("#header .region-hoofdmenu .om-menu").addClass("non-active");
            $("#header").append('<div id="hamburger"></div>');
            $( "#block-multiblock-1" ).insertBefore( $( "#block-block-1 p:first-of-type + p.rood" ) );
        });
        $(function() {      
           $( ".node-diensten-energiebesparen .field-name-field-body2" ).before( $( "#block-webform-client-block-210" ) );
        });    
        $(function() {      
            $("#hamburger").click(function() {
                var menuhoogtemobielall = 0;
                $('#header .region-hoofdmenu .om-menu li').each(function() {menuhoogtemobielall += $(this).height();});
                $("#header .region-hoofdmenu .om-menu").css('height', ((menuhoogtemobielall)) + 'px' );
                $("#header .region-hoofdmenu .om-menu").toggleClass("non-active");
              });
            $(".plus").click(function() {
                var menuhoogtemobielall = 0;
                $('#header .region-hoofdmenu .om-menu li').each(function() {menuhoogtemobielall += $(this).height();});
                $("#header .region-hoofdmenu .om-menu").css('height', ((menuhoogtemobielall)) + 'px' );
              });
        });
        $(function() {
            $('.view-id-projecten_overzicht .views-row').matchHeight();
            $('.view-diensten .views-row').matchHeight();
        });
        $(function() { 
          $(".om-leaf").hover(function() {
              $('#header .om-leaf .om-maximenu-content').css('display', 'none' ).css('z-index', '-1' );
              $(this).find('.om-maximenu-content').css('display', 'block' ).css('z-index', '99' );
          });
        });
        $(function() {
            if ($("body").hasClass("front")) {
              // groene cloud maken
              $("#cloud").hover(function() {
                $(this).toggleClass("actief");
              });
              $( "#cloud input").focus(function() {
                $("#cloud").addClass("focus");
              });
              $("#cloud input").blur(function(){
                 $("#cloud").removeClass("focus");
              });
              window.setInterval(function(){
                if (!$('#cloud').hasClass('actief') && !$('#cloud').hasClass('focus')) {
                  if ($('#cloudcheck121, #cloudcheck244, #cloudcheck243, #cloudcheck242, #cloudcheck241').is(':visible')) {
                      $("#cloud").addClass("energie");
                      $("#cloud").removeClass("andere");
                  }
                  else {
                      $("#cloud").removeClass("energie");
                      $("#cloud").addClass("andere");
                  }
                }
              }, 500);
            }
          });
        
    </script>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-42207175-11', 'fledderman.nl');
  ga('send', 'pageview');

</script>
    
    
    <script type="text/javascript">
    /*$(document).ready(function() {
        $("#header li").mouseover(function() {
             var menuliwidth = $('#header .om-menu .view-menus li').width();
            $("#header .om-menu .view-menus ul").css('width', ((menuliwidth*2)) + 'px' );
            $("#header .om-menu .view-menus ul li").css('width', ((menuliwidth)) + 'px' );
        });
    });*/
</script>
   
    <script>
        $(document).ready(function(){
          if ($("body").hasClass("section-blog")) {
            $('.webform-hints input, .webform-hints textarea').placeholder();
          }
          $("#edit-mergevars-email").attr("placeholder", "Email");
         });
        $(document).ready(function(){

                /*$( ".gm-style-iw" ).each(function( index ) {
                  setTimeout(function(){
                  (this).addClass("wittepopup");
                }, 1000);
                  alert('yes');
              });*/

                $('.view-projecten').each(function(){  
            
                    var highestBox = 0;
                    $('.views-row', this).each(function(){
            
                        if($(this).height() > highestBox) 
                           highestBox = $(this).height(); 
                    });  
            
                    $('.views-row',this).height(highestBox);
            
                });  
            });
    </script>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
  <?php if ($skip_link_text && $skip_link_anchor): ?>
    <p id="skip-link">
      <a href="#<?php print $skip_link_anchor; ?>" class="element-invisible element-focusable"><?php print $skip_link_text; ?></a>
    </p>
  <?php endif; ?>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</body>
</html>
