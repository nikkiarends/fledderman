<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>
<div class="foto" style="background:url('<?php print $output; ?>');background-size: cover;background-position: center;">
    <div class="whiteover">
    <div class="grid grid-pad">
        <div class="c-12">
            <div class="contentvak">
                <div id="logo">
                    <a href="/">
                        <object data="<?php print path_to_theme(); ?>/images/fledderman-logo.svg" type="image/svg+xml">
                            <img src="<?php print path_to_theme(); ?>/images/fledderman-logo.png" alt="Fledderman logo" />
                        </object>
                        <img src="<?php print path_to_theme(); ?>/images/fledderman-logo.png" alt="Fledderman logo" width="354" height="91" />
                    </a>
                </div>
            </div>
        </div>
    </div>
        </div>
</div>
