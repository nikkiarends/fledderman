<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */
?>
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyDt3dyOCaQZdbAer2bP7HtZdhimmjo98G8&sensor=false&language=nl"></script>
    <script type="text/javascript" src="/sites/all/themes/fledderman/js/googlemaps.js"></script>

<div id="pusher">
            
            <div id="contactfloat">
                <h3>Bouwbedrijf Fledderman</h3>
                Hoofdweg 90<br />
                9628 CR Siddeburen<br />
                T: 0598 432 444<br />
                F: 0598 432 774<br />
                E: <a href="mailto:info@fledderman.nl">info@fledderman.nl</a><br />
                <h3>Meld je aardbevingschade</h3>
                T: 0598 432 444
            </div>
            
    <?php if ($page['hoofdmenu']): ?>
            <div id="header">
                <div class="grid grid-pad">
                    <div class="c-12">
                        <div class="contentvak">
                            <?php print render($page['hoofdmenu']); ?>
                        </div>
                    </div>
                </div>
            </div>
    <?php endif; ?>
    
    <div id="map-canvas"></div>
            
            <div id="inhoud">
                <div class="grid grid-pad">
                    <?php if ($page['contactleft']): ?>
                        <div class="contactleft">
                                <?php print render($page['contactleft']); ?>
                        </div>
                    <?php endif; ?>
                    <div id="contentblock" class="column" role="main">
                      <?php print render($page['highlighted']); ?>
                      <?php // print $breadcrumb; ?>
                      <a id="main-content"></a>
                      <?php print render($title_prefix); ?>
                      <?php if ($title): ?>
                        <h1 class="page__title title" id="page-title"><?php print $title; ?></h1>
                      <?php endif; ?>
                      <?php print render($title_suffix); ?>
                      <?php print $messages; ?>
                      <?php print render($tabs); ?>
                      <?php print render($page['help']); ?>
                      <?php if ($action_links): ?>
                        <ul class="action-links"><?php print render($action_links); ?></ul>
                      <?php endif; ?>
                      <?php print render($page['content']); ?>
                      <?php print $feed_icons; ?>
                    </div>
                </div><!-- end inhoud -->
                <?php if ($page['meertestimonials']): ?>
                    <div class="meertestimonials">
                        <div class="grid grid-pad">
                            <?php print render($page['meertestimonials']); ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>

    
        </div>
        <div id="footer">
            <div class="innerfooter">
                <div class="grid grid-pad">
                    <?php if ($page['adres']): ?>
                        <?php print render($page['adres']); ?>
                    <?php endif; ?>
                    <div class="c-1 vrij footertop">
                        <div class="contentvak">
                            &nbsp;
                        </div>
                    </div>
                    <div class="c-3 sitemap footertop">
                        <div class="contentvak">
                            <?php if ($page['footermenu']): ?>
                                <?php print render($page['footermenu']); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="c-3 projecten footertop">
                        <div class="contentvak">
                            <?php if ($page['footerprojecten']): ?>
                                <?php print render($page['footerprojecten']); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="c-12 footermiddle logos">
                        <div class="contentvak">
                            <img src="<?php echo $front_page ?>sites/all/themes/fledderman/images/bouwgarant.png" alt="bouwgarant">
                            <img src="<?php echo $front_page ?>sites/all/themes/fledderman/images/debouwmaakthet.png" alt-="de bouw maakt het">
                            <img src="<?php echo $front_page ?>sites/all/themes/fledderman/images/vca-p.png" alt-="VCA">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="footerbottom">
            <div class="grid grid-pad">
                 <div class="c-6 copyright footerbottom">
                    <div class="contentvak">
                        &copy; Copyright 2014 Bouwbedrijf Fledderman | <a href="<?php echo $front_page ?>privacy-statement">Privacy Statement</a>
                    </div>
                </div>
                <div class="c-6 madeby footerbottom">
                    <div class="contentvak">
                        Ontwerp &amp; Realisatie: <a href="http://www.publiek.com" target="new">Publiek</a> &amp; <a href="http://www.eenvormvan.nl" target="new">eenVormvan</a>
                    </div>
                </div>
            </div>
        </div>