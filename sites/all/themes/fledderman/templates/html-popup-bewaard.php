<div id="popupmelding" class="actief">
      <div class="popuptekst">
        <div class="popupinhoud">
          <div class="popupboven">
              <span class="popuptitel1">Heeft u nog recht op €4000,- subsidie?</span><br /><br />
              <br />
              Lees hier alles over de veranderingen en maak een afspraak<Br />
              om de mogelijkheden te bekijken. 
              <div class="popupsluiten2"></div>
            </div>
            <div class="popuponder">
              <div class="popupsluiten leesmeer">Meer informatie</div>
           </div>
        </div>
      </div>
    </div>
  <script>
    var viewportHeight = $(window).height();
    var hoofdmenu = $(".popuptekst").outerHeight();
    var berekend = (viewportHeight - hoofdmenu) / 2;
    $(".popuptekst").css("margin-top", berekend +"px");

    $("#popupmelding").click(function() {
      //$("body").toggleClass("scroll");
      $("#popupmelding").removeClass("actief");
    });
  </script>